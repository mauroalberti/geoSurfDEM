//
// Created by mauro on 08/01/23.
// from Mathemathinking

#include<random>
#include<cmath>
#include<chrono>
#include <iostream>

#include <sstream>    // header file for stringstream
#include <string>

using namespace std;


int main(int argc, char *argv[]) {

    // Set up random number generators

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 generator (seed);
    std::uniform_real_distribution<double> uniform01(0.0, 1.0);

    // simulation parameters

    int* N = new int[9] {3, 5, 10, 50, 100, 500, 1000, 5000, 10000};
    int num_replicas = 100;

    //

    for (int in = 0; in < 9; in++) {

        int n = N[in];

        for (int i=0; i<num_replicas; i++){

            FILE* correct;

            stringstream ss;
            ss << "./simulations/sim_" << n << "_" << i << ".csv";
            string s = ss.str();

            correct = fopen(s.c_str(), "w");
            fprintf(correct, "x,y,z,Theta,Phi\n");

            for (int r = 0; r < n; r++) {

                double theta = 2 * M_PI * uniform01(generator);
                double phi = acos(1 - 2 * uniform01(generator));
                double x = sin(phi) * cos(theta);
                double y = sin(phi) * sin(theta);
                double z = cos(phi);

                fprintf(correct, "%.14f,%.14f,%.14f,%f,%f\n", x, y, z, theta, phi);

            }

            fclose(correct);

        }

    }

}
